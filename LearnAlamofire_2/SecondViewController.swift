//
//  SecondViewController.swift
//  LearnAlamofire_2
//
//  Created by Klevis Mehmeti on 06/01/17.
//  Copyright © 2017 Klevis Mehmeti. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {
    var info : Info!
    
    
    @IBOutlet weak var title2: UILabel!
    @IBOutlet weak var iduser2: UILabel!
    @IBOutlet weak var id2: UILabel!
        @IBOutlet weak var nody2: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        id2.text = "\(info.idInfo!)"
        title2.text = info.titleInfo
        nody2.text = info.bodyInfo
        iduser2.text = "\(info.idUserInfo!)"
        
    }
   
}
