import UIKit
import Alamofire

import SwiftyJSON
import AlamofireImage
class ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    
    
    @IBOutlet weak var tableView: UITableView!
    var info = [Info]()
    var imgWeb :UIImage!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Alamofire.request("http://77.242.25.43:8080/girogama/img/c/6.jpg").responseImage { response in
            debugPrint(response)
            
            
            
            if let image = response.result.value {
                
                self.imgWeb = image
                print(" a u kalua\(self.imgWeb)")
            }
            self.tableView.reloadData()
        }
        
        
        
        
        
        
        
        Alamofire.request("https://jsonplaceholder.typicode.com/posts").responseJSON { response in
            
            
            let json = response.result.value
            let swiftyJsonVar = JSON(json!)
            
            for a in 0 ... swiftyJsonVar.count {
                let currentInfo = Info()
                
                currentInfo.titleInfo = swiftyJsonVar[a]["title"].string
                currentInfo.idInfo = swiftyJsonVar[a]["id"].int
                currentInfo.idUserInfo = swiftyJsonVar[a]["userId"].int
                currentInfo.bodyInfo = swiftyJsonVar[a]["body"].string
                
                self.info.append(currentInfo)
            }
            
            self.tableView.reloadData()
            
            
            
        }
        
        tableView.dataSource = self
        //  tableView.register(LabelCell.self, forCellReuseIdentifier: "LabelCell")
        //tableView.register(UINib(nibName: "LabelCell",bundle: Bundle.main), forCellReuseIdentifier: "LabelCell")
        tableView.delegate = self
        
        tableView.reloadData()
    }
    
    
    
    
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return info.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "LabelCell", for: indexPath) as! LabelCell
        //  cell.idLabel.text = info[indexPath.row].idInfo
        //  cell.iduserLable.text = info[indexPath.row].idUserInfo
        cell.LabelText.text = info[indexPath.row].titleInfo
        cell.img.image = imgWeb
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      var  infoTransport : Info!
        tableView.deselectRow(at: indexPath, animated: true)
        infoTransport = info[indexPath.row]
        performSegue(withIdentifier: "SegueSecondView", sender: infoTransport)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "SegueSecondView"{
            if let secondView = segue.destination as? SecondViewController{
                if let infoTransport = sender as? Info{
                secondView.info = infoTransport
                
                }
             }
        }
    }
   
    
}
