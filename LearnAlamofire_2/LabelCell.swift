//
//  LabelCell.swift
//  LearnAlamofire_2
//
//  Created by Klevis Mehmeti on 06/01/17.
//  Copyright © 2017 Klevis Mehmeti. All rights reserved.
//

import UIKit

class LabelCell: UITableViewCell {

    @IBOutlet weak var LabelText: UILabel!
    @IBOutlet weak var img:UIImageView!

}
